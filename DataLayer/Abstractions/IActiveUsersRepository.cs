﻿using DataLayer.Models;

namespace DataLayer.Abstractions
{
    public interface IActiveUsersRepository
    {
        Task<IEnumerable<ActiveUserEntity>> GetAllAsync(CancellationToken cancellationToken);
        Task<ActiveUserEntity?> GetByIdAsync(Guid userId, CancellationToken cancellationToken);
        Task AddOrUpdateAsync(ActiveUserEntity entity, CancellationToken cancellationToken);
        Task DeleteAsync(Guid userId, CancellationToken cancellationToken);
    }
}
