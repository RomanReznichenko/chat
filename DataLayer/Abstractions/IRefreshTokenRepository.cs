﻿using DataLayer.Models;

namespace DataLayer.Abstractions
{
    public interface IRefreshTokenRepository
    {
        Task<RefreshTokenEntity?> GetByIdAsync(Guid id, CancellationToken cancellationToken);
        Task<RefreshTokenEntity> AddAsync(RefreshTokenEntity entity, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}
