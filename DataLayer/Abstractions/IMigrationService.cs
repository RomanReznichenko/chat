﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Abstractions
{
    public interface IMigrationService
    {
        Task AddMigration();
    }
}
