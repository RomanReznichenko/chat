﻿using DataLayer.Abstractions;
using DataLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataLayer.Configuration
{
    public static class DataLayerConfiguration
    {
        private const string ConnectionString = "DbConnectionString";

        public static IServiceCollection AddDataLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DbContext, TestAssignmentDbContext>((DbContextOptionsBuilder options) =>
            {
                var connectionString = configuration.GetConnectionString(ConnectionString);

                options.UseNpgsql(connectionString);
            });

            services.AddTransient<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddTransient<IActiveUsersRepository, ActiveUsersRepository>();

            services.AddTransient<IMigrationService, MigrationService>();

            return services;
        }
    }
}
