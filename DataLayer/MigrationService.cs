﻿using DataLayer.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class MigrationService : IMigrationService
    {
        private readonly DbContext _context;

        public MigrationService(DbContext context)
        {
            _context = context;
        }

        public async Task AddMigration()
        {
            await _context.Database.MigrateAsync();
        }
    }
}
