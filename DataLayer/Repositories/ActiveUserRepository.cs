﻿using DataLayer.Abstractions;
using DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Repositories
{
    public class ActiveUsersRepository : IActiveUsersRepository
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<ActiveUserEntity> _activeUserEntities;

        public ActiveUsersRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _activeUserEntities = _dbContext.Set<ActiveUserEntity>();
        }


        public async Task AddOrUpdateAsync(ActiveUserEntity entity, CancellationToken cancellationToken)
        {
            var activeUserEntity = await _activeUserEntities.FirstOrDefaultAsync(x => x.UserId == entity.UserId);

            if (activeUserEntity == null)
            {
                await _activeUserEntities.AddAsync(entity);
            }
            else
            {
                activeUserEntity.SessionId = entity.SessionId;
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(Guid userId, CancellationToken cancellationToken)
        {
            var activeUserEntity = await _activeUserEntities.FirstOrDefaultAsync(x => x.UserId == userId);
            if (activeUserEntity == null)
            {
                return;
            }

            _activeUserEntities.Remove(activeUserEntity);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<IEnumerable<ActiveUserEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await _activeUserEntities.Include(u => u.User).ToListAsync(cancellationToken);
        }

        public Task<ActiveUserEntity?> GetByIdAsync(Guid userId, CancellationToken cancellationToken)
        {
            return _activeUserEntities.Include(u => u.User).FirstOrDefaultAsync(x => x.UserId == userId);
        }
    }
}
