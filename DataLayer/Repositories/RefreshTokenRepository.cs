﻿using DataLayer.Abstractions;
using DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Repositories
{
    public class RefreshTokenRepository : IRefreshTokenRepository
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<RefreshTokenEntity> _refreshTokenEntities;

        public RefreshTokenRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _refreshTokenEntities = _dbContext.Set<RefreshTokenEntity>();
        }

        public async Task<RefreshTokenEntity?> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _refreshTokenEntities.FindAsync(new object[] { id }, cancellationToken);
        }

        public async Task<RefreshTokenEntity> AddAsync(RefreshTokenEntity entity, CancellationToken cancellationToken)
        {
            await _refreshTokenEntities.AddAsync(entity, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return entity;

        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var tokenEntities = await _refreshTokenEntities.FirstOrDefaultAsync(x => x.Id == id);
            if (tokenEntities == null)
            {
                return;
            }

            _refreshTokenEntities.Remove(tokenEntities);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
