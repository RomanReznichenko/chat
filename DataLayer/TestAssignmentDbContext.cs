﻿using DataLayer.Models;
using DataLayer.Models.Configurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataLayer
{
    internal class TestAssignmentDbContext : IdentityDbContext<UserEntity, IdentityRole<Guid>, Guid>
    {
        public DbSet<RefreshTokenEntity> RefreshTokens { get; set; }
        public DbSet<ActiveUserEntity> ActiveUsers { get; set; }

        public TestAssignmentDbContext(DbContextOptions<TestAssignmentDbContext> dbContextOptions) : base(dbContextOptions)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new UserEntityConfiguration());
        }
    }
}
