﻿namespace DataLayer.Models
{
    public class ActiveUserEntity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid SessionId { get; set; }
        public string ConnecyionId { get; set; }
        public UserEntity User { get; set; }
    }
}
