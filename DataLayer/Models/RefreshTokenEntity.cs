﻿namespace DataLayer.Models
{
    public class RefreshTokenEntity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserEntity User { get; set; }
        public DateTimeOffset Expires { get; set; }
        public DateTimeOffset Created { get; set; }
        public string Token { get; set; }
        public bool IsExpired => DateTimeOffset.UtcNow >= Expires;

    }
}
