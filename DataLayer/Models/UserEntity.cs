﻿using Microsoft.AspNetCore.Identity;

namespace DataLayer.Models
{
    public class UserEntity : IdentityUser<Guid>
    {
        public string NickName { get; set; }
        public List<RefreshTokenEntity> RefreshTokens { get; set; }
    }
}
