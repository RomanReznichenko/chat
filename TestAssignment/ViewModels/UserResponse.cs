﻿using DataLayer.Models;

namespace TestAssignment.ViewModels
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string Emeil { get; set; }
        public string NickName { get; set; }

        public UserResponse(UserEntity user)
        {
            Id = user.Id;
            Emeil = user.Email;
            NickName = user.NickName;
        }

    }
}
