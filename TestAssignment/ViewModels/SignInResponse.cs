﻿namespace TestAssignment.ViewModels
{
    public class SignInResponse
    {
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public SignInResponse(string accessToken, string refreshToken)
        {
            RefreshToken = refreshToken;
            AccessToken = accessToken;
        }
    }
}
