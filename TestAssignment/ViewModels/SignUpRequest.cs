﻿namespace TestAssignment.ViewModels
{
    public class SignUpRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string NickName { get; set; }
    }
}
