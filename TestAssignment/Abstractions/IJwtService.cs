﻿using DataLayer.Models;

namespace TestAssignment.Abstractions
{
    public interface IJwtService
    {
        Task<(string, string)> GenerateTokensAsync(UserEntity user, CancellationToken cancellationToken);
        Task<(string, string)> RefreshTokensAsync(Guid expiredTokenId, CancellationToken cancellationToken);
        Task DeactivateRefreshTokenAsync(Guid tokenId, CancellationToken cancellationToken);
    }
}
