﻿namespace TestAssignment.Abstractions
{
    public interface IChatService
    {
        Task SetUserOnlineAsync(Guid userId, string connectionId, CancellationToken cancellationToken);
        Task SetUserOfflineAsync(Guid userId, CancellationToken cancellationToken);
        Task SendInviteRequestAsync(Guid addressee, Guid sender, CancellationToken cancellationToken);
        Task SendInviteResponseAsync(Guid addressee, Guid sender, object body, CancellationToken cancellationToken);
        Task SendMessageAsync(Guid addressee, Guid sender, object body, CancellationToken cancellationToken);
    }
}
