﻿using TestAssignment.Models;

namespace TestAssignment.Abstractions
{
    public interface IMessageSender
    {
        public static Guid CurrentInstanceId { get; } = Guid.NewGuid();
        Task SeendMessageAsync(Message message, CancellationToken cancellationToken);
    }
}
