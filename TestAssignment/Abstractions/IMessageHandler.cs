﻿using TestAssignment.Models;

namespace TestAssignment.Abstractions
{
    public interface IMessageHandler
    {
        Task HandleMessage(Message message, CancellationToken cancellationToken);
    }
}
