﻿using DataLayer.Models;

namespace TestAssignment.Abstractions
{
    public interface IActiveUsersService
    {
        Task<IEnumerable<ActiveUserEntity>> GetActiveUsersWithoutUser(Guid currentUserId, CancellationToken cancellationToken);
        Task<ActiveUserEntity?> GetUserByIdAsync(Guid userId, CancellationToken cancellationToken);
        Task AddAsync(Guid userId, Guid instanceId, string connectionId, CancellationToken cancellationToken);
        Task RemoveAsync(Guid userId, CancellationToken cancellationToken);
    }
}
