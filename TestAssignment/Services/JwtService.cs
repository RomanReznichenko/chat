﻿using DataLayer.Abstractions;
using DataLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TestAssignment.Abstractions;
using TestAssignment.Constants;
using TestAssignment.Models;

namespace TestAssignment.Services
{
    public class JwtService : IJwtService
    {
        private readonly UserManager<UserEntity> _userManager;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly TokensOptions _tokensOptions;

        public JwtService(
            IOptions<TokensOptions> options,
            UserManager<UserEntity> userManager,
            IRefreshTokenRepository refreshTokenRepository)
        {
            _userManager = userManager;
            _refreshTokenRepository = refreshTokenRepository;
            _tokensOptions = options.Value;
        }

        public async Task<(string, string)> GenerateTokensAsync(UserEntity user, CancellationToken cancellationToken)
        {
            var (refreshToken, id) = await GenerateRefreshToken(user, cancellationToken);
            var accessToken = await GenerateAccessToken(user, id, cancellationToken);

            return (accessToken, refreshToken);
        }

        public async Task<(string, string)> RefreshTokensAsync(Guid expiredTokenId, CancellationToken cancellationToken)
        {
            var expiredTokenEntity = await _refreshTokenRepository.GetByIdAsync(expiredTokenId, cancellationToken);

            if (expiredTokenEntity is null)
            {
                throw new Exception("Can't fint refresh token.");
            }

            if (expiredTokenEntity.IsExpired)
            {
                await DeactivateRefreshTokenAsync(expiredTokenId, cancellationToken);
                throw new Exception("Refresh token is expired.");
            }

            var user = await _userManager.FindByIdAsync(expiredTokenEntity.UserId.ToString());
            var tokens = await GenerateTokensAsync(user, cancellationToken);

            await _refreshTokenRepository.DeleteAsync(expiredTokenId, cancellationToken);

            return tokens;
        }

        public async Task DeactivateRefreshTokenAsync(Guid tokenId, CancellationToken cancellationToken)
        {
            await _refreshTokenRepository.DeleteAsync(tokenId, cancellationToken);
        }

        private Task<string> GenerateAccessToken(UserEntity user, Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var key = Encoding.ASCII.GetBytes(_tokensOptions.Access.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] 
                {
                    new Claim(ClaimTypes.Sid, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                    new Claim(ClaimNames.RefreshTokenSid, id.ToString()),
                }),

                Expires = DateTime.UtcNow.AddMinutes(_tokensOptions.Access.LifeTime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            

            return Task.FromResult(GenerateToken(tokenDescriptor));
        }

        private async Task<(string, Guid)> GenerateRefreshToken(UserEntity user, CancellationToken cancellationToken)
        {
            var tokenId = Guid.NewGuid();
            var key = Encoding.ASCII.GetBytes(_tokensOptions.Refresh.Secret);
            var expires = DateTime.UtcNow.AddMinutes(_tokensOptions.Refresh.LifeTime);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] 
                {
                    new Claim("rsid", tokenId.ToString())
                }),
                Expires = expires,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            };

            var token = GenerateToken(tokenDescriptor);

            await _refreshTokenRepository.AddAsync(new RefreshTokenEntity()
            {
                Id = tokenId,
                Created = DateTime.UtcNow,
                UserId = user.Id,
                Token = token,
                Expires = expires

            }, cancellationToken);

            return (token, tokenId);
        }

        private string GenerateToken(SecurityTokenDescriptor tokenDescriptor)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
