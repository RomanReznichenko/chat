﻿using DataLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using TestAssignment.Abstractions;
using TestAssignment.Models;

namespace TestAssignment.Services
{
    public class MessageHandler : IMessageHandler
    {
        private readonly IHubContext<ChatHub> _chatHub;
        private readonly IActiveUsersService _activeUsersService;
        private readonly UserManager<UserEntity> _userManager;

        public MessageHandler(IHubContext<ChatHub> chatHub,
            IActiveUsersService activeUsersService,
            UserManager<UserEntity> userManager)
        {
            _chatHub = chatHub;
            _activeUsersService = activeUsersService;
            _userManager = userManager;
        }

        public Task HandleMessage(Message message, CancellationToken cancellationToken)
        {
            switch (message.MessageType)
            {
                case MessageType.InviteRequest: 
                    return HandleInviteRequest(message, cancellationToken);
                case MessageType.InviteResponse: 
                    return HandleInviteResponse(message, cancellationToken);
                default: 
                    return HandleChatMessage(message, cancellationToken);
            }
        }

        private async Task HandleInviteRequest(Message message, CancellationToken cancellationToken)
        {
            var activeUser = await _activeUsersService.GetUserByIdAsync(message.Addressee!.Value, cancellationToken);
            if (activeUser == null)
            {
                return;
            }

            var sender = await _userManager.FindByIdAsync(message.Sender.ToString());
            var d = _chatHub.Clients.Client(activeUser.ConnecyionId);

            await _chatHub.Clients.Client(activeUser.ConnecyionId).SendAsync("InviteRequest", new Sender(sender), cancellationToken);
        }

        private async Task HandleInviteResponse(Message message, CancellationToken cancellationToken)
        {
            var activeUser = await _activeUsersService.GetUserByIdAsync(message.Addressee!.Value, cancellationToken);
            if (activeUser == null)
            {
                return;
            }

            var sender = await _userManager.FindByIdAsync(message.Sender.ToString());
            await _chatHub.Clients.Client(activeUser.ConnecyionId).SendAsync("InviteResponse", new { sender = new Sender(sender), status = message.Body }, cancellationToken);
        }

        private async Task HandleChatMessage(Message message, CancellationToken cancellationToken)
        {
            var activeUser = await _activeUsersService.GetUserByIdAsync(message.Addressee!.Value, cancellationToken);
            if (activeUser == null)
            {
                return;
            }

            await _chatHub.Clients.Client(activeUser.ConnecyionId).SendAsync("ReceiveMessage", message.Body, cancellationToken);

        }

        
    }
}
