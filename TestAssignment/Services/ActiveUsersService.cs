﻿using DataLayer.Abstractions;
using DataLayer.Models;
using TestAssignment.Abstractions;

namespace TestAssignment.Services
{
    public class ActiveUsersService : IActiveUsersService
    {
        private readonly IActiveUsersRepository _activeUsersRepository;

        public ActiveUsersService(
            IActiveUsersRepository activeUsersRepository)
        {
            _activeUsersRepository = activeUsersRepository;
        }

        public async Task AddAsync(Guid userId, Guid instanceId, string connectionId, CancellationToken cancellationToken)
        {
            var entity = new ActiveUserEntity()
            {
                UserId = userId,
                SessionId = instanceId,
                ConnecyionId = connectionId
            };

            await _activeUsersRepository.AddOrUpdateAsync(entity, cancellationToken);
        }

        public async Task<IEnumerable<ActiveUserEntity>> GetActiveUsersWithoutUser(Guid currentUserId, CancellationToken cancellationToken)
        {
            var allUsers = await _activeUsersRepository.GetAllAsync(cancellationToken);
            return allUsers.Where(u => u.UserId != currentUserId);
        }

        public Task<ActiveUserEntity?> GetUserByIdAsync(Guid userId, CancellationToken cancellationToken)
        {
            return _activeUsersRepository.GetByIdAsync(userId, cancellationToken);
        }

        public async Task RemoveAsync(Guid userId, CancellationToken cancellationToken)
        {
            await _activeUsersRepository.DeleteAsync(userId, cancellationToken);
        }
    }
}
