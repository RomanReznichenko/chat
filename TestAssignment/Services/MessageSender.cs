﻿using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Options;
using TestAssignment.Abstractions;
using TestAssignment.Models;

namespace TestAssignment.Services
{
    public class MessageSender : IMessageSender
    {

        private readonly QueueOptions _queueOptions;
        private readonly ServiceBusClient _busClient;
        private readonly ServiceBusSender _busSender;

        public MessageSender(IOptions<QueueOptions> options)
        {
            _queueOptions = options.Value;

            _busClient = new ServiceBusClient(_queueOptions.ConnectionString);
            _busSender = _busClient.CreateSender(_queueOptions.QueueName);
        }


        public async Task SeendMessageAsync(Message message, CancellationToken cancellationToken)
        {
            var busMessage = new ServiceBusMessage()
            {
                SessionId = IMessageSender.CurrentInstanceId.ToString(),
                Body = new BinaryData(message),
            };

            await _busSender.SendMessageAsync(busMessage, cancellationToken);
        }
    }
}
