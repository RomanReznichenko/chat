﻿using TestAssignment.Abstractions;
using TestAssignment.Models;

namespace TestAssignment.Services
{
    public class ChatService : IChatService
    {
        private readonly IActiveUsersService _activeUsersService;
        private readonly IMessageSender _messageSender;

        public ChatService(IActiveUsersService activeUsersService,
            IMessageSender messageSender)
        {
            _activeUsersService = activeUsersService;
            _messageSender = messageSender;
        }

        public async Task SendInviteRequestAsync(Guid addressee, Guid sender, CancellationToken cancellationToken)
        {
            var message = new Message()
            {
                Addressee = addressee,
                Sender = sender,
                MessageType = MessageType.InviteRequest
            };

            await SendAsync(message, cancellationToken);
        }

        public async Task SendInviteResponseAsync(Guid addressee, Guid sender, object body, CancellationToken cancellationToken)
        {
            var message = new Message()
            {
                Addressee = addressee,
                Sender = sender,
                Body = body,
                MessageType = MessageType.InviteResponse
            };

            await SendAsync(message, cancellationToken);
        }

        public async Task SendMessageAsync(Guid addressee, Guid sender, object body, CancellationToken cancellationToken)
        {
            var message = new Message()
            {
                Addressee = addressee,
                Sender = sender,
                Body = body,
                MessageType = MessageType.ChatMessage
            };

            await SendAsync(message, cancellationToken);
        }

        public async Task SetUserOfflineAsync(Guid userId, CancellationToken cancellationToken)
        {
            await _activeUsersService.RemoveAsync(userId, cancellationToken);
        }

        public async Task SetUserOnlineAsync(Guid userId, string connectionId, CancellationToken cancellationToken)
        {
            await _activeUsersService.AddAsync(userId, IMessageSender.CurrentInstanceId, connectionId, default);
        }

        private async Task SendAsync(Message message, CancellationToken cancellationToken)
        {
            await _messageSender.SeendMessageAsync(message, cancellationToken);
        }
    }
}
