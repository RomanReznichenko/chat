﻿using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Options;
using TestAssignment.Abstractions;
using TestAssignment.Models;

namespace TestAssignment.Services
{
    public class MessageReceiver : BackgroundService
    {
        private readonly QueueOptions _queueOptions;
        private readonly IServiceProvider _serviceProvider;

        public MessageReceiver(IOptions<QueueOptions> options, IServiceProvider serviceProvider)
        {
            _queueOptions = options.Value;
            _serviceProvider = serviceProvider;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await using var client = new ServiceBusClient(_queueOptions.ConnectionString);

            var receiver = await client.AcceptSessionAsync(_queueOptions.QueueName, IMessageSender.CurrentInstanceId.ToString(), cancellationToken: stoppingToken);

            while (!stoppingToken.IsCancellationRequested)
            {
                var receivedMessage = await receiver.ReceiveMessageAsync();
                if (receivedMessage != null)
                {
                    _ = HandleMessage(receivedMessage, receiver, stoppingToken);
                }
            }
        }

        private async Task HandleMessage(ServiceBusReceivedMessage receivedMessage, ServiceBusSessionReceiver receiver, CancellationToken cancellationToken)
        {
            try
            {
                using var scope = _serviceProvider.CreateScope();
                var messageHandler = scope.ServiceProvider.GetRequiredService<IMessageHandler>();
                var body = receivedMessage.Body.ToObjectFromJson<Message>();
                await messageHandler.HandleMessage(body, cancellationToken);
                await receiver.CompleteMessageAsync(receivedMessage, cancellationToken);
            }
            catch(Exception ex)
            {
                await receiver.AbandonMessageAsync(receivedMessage, cancellationToken: cancellationToken);
            }
        }
    }
}
