﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;
using TestAssignment.Abstractions;
using TestAssignment.Models;

namespace TestAssignment.Services
{
    [Authorize("sid")]
    public class ChatHub : Hub
    {
        private readonly IChatService _chatService;

        public ChatHub(IChatService chatService)
        {
            _chatService = chatService;
        }

        public async Task SendAsync(string addressee, string message)
        {
            var currentUserId = GetCurrentUserId();
            var addresseeGuid = Guid.Parse(addressee);
            await _chatService.SendMessageAsync(addresseeGuid, currentUserId, message, default);
        }

        public async Task InitiateAsync(string addressee)
        {
            var currentUserId = GetCurrentUserId();
            var addresseeGuid = Guid.Parse(addressee);
            await _chatService.SendInviteRequestAsync(addresseeGuid, currentUserId, default);
            await Task.CompletedTask;
        }

        public async Task InviteResponse(string addressee, InviteResponse response)
        {
            var currentUserId = GetCurrentUserId();
            var addresseeGuid = Guid.Parse(addressee);
            await _chatService.SendInviteResponseAsync(addresseeGuid, currentUserId, response, default);
        }

        public override async Task OnConnectedAsync()
        {
            var currentUserId = GetCurrentUserId();
            await _chatService.SetUserOnlineAsync(currentUserId, Context.ConnectionId, default);
            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception? exception)
        {
            var currentUserId = GetCurrentUserId();
            await _chatService.SetUserOfflineAsync(currentUserId, default);
            await base.OnDisconnectedAsync(exception);
        }

        private Guid GetCurrentUserId()
        {
            var id = Context.User.FindFirstValue(ClaimTypes.Sid);

            return Guid.Parse(id);
        }
    }
}
