﻿namespace TestAssignment.Models
{
    public enum InviteResponse
    {
        Declined = 0,
        Approved
    }
}
