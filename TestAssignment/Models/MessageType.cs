﻿namespace TestAssignment.Models
{
    public enum MessageType
    {
        InviteRequest = 0,
        InviteResponse,
        ChatMessage
    }
}
