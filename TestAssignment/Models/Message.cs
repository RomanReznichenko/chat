﻿namespace TestAssignment.Models
{
    public class Message
    {
        public Guid? Addressee { get; set; }
        public Guid? Sender { get; set; }
        public object Body { get; set; }
        public MessageType MessageType { get; set; }
    }
}
