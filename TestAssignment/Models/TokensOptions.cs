﻿namespace TestAssignment.Models
{
    public class TokensOptions
    {
        public const string ConfigurationKey = "TokensOptions";
        public Options Access { get; set; }
        public Options Refresh { get; set; }

        public class Options
        {
            public int LifeTime { get; set; }
            public string Secret { get; set; }
        }
    }
}
