﻿namespace TestAssignment.Models
{
    public class QueueOptions
    {
        public const string ConfigurationKey = "QueueOptions";
        public string QueueName { get; set; }
        public string ConnectionString { get; set; }
    }
}
