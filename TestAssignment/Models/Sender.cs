﻿using DataLayer.Models;

namespace TestAssignment.Models
{
    public class Sender
    {
        public Guid Id { get; set; }
        public string NickName { get; set; }

        public Sender(UserEntity user)
        {
            Id = user.Id;
            NickName = user.NickName;
        }
    }
}
