using DataLayer.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using TestAssignment.Abstractions;
using TestAssignment.Models;
using TestAssignment.Services;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;
var configuration = builder.Configuration;

services.AddControllers();

services.AddDataLayer(configuration);
services.AddIdentity();

services.AddOptions<TokensOptions>()
    .Bind(configuration.GetSection(TokensOptions.ConfigurationKey));

services.AddOptions<QueueOptions>()
    .Bind(configuration.GetSection(QueueOptions.ConfigurationKey));

services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(options =>
    {
        var secrete = configuration.GetValue<string>("TokensOptions:Access:Secret");
        var key = Encoding.ASCII.GetBytes(secrete);

        options.SaveToken = true;
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            IssuerSigningKey = new SymmetricSecurityKey(key)
        };

        options.Events = new JwtBearerEvents
        {
            OnMessageReceived = context =>
            {
                var accessToken = context.Request.Query["access_token"];

                // If the request is for our hub...
                var path = context.HttpContext.Request.Path;
                if (!string.IsNullOrEmpty(accessToken) &&
                    (path.StartsWithSegments("/messaging")))
                {
                    // Read the token out of the query string
                    context.Token = accessToken;
                }
                return Task.CompletedTask;
            }
        };
    });

services.AddAuthorization(options =>
{
    options.AddPolicy("sid", builder =>
    {
        builder.RequireClaim(ClaimTypes.Sid);
    });
});
services.AddSignalR(options => options.EnableDetailedErrors = true);
services.AddHostedService<MessageReceiver>();

services.AddSingleton<IMessageSender, MessageSender>();

services.AddScoped<IActiveUsersService, ActiveUsersService>();
services.AddScoped<IMessageHandler, MessageHandler>();
services.AddScoped<IChatService, ChatService>();

services.AddTransient<IJwtService, JwtService>();

var app = builder.Build();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapHub<ChatHub>("/messaging", options =>
    {
        options.Transports = HttpTransportType.WebSockets |
                             HttpTransportType.LongPolling;
    });
});

app.UseCors(builder => builder.WithOrigins("*"));

await app.ApplyMigrations();

app.Run();
