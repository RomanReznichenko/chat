﻿using DataLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;
using TestAssignment.Abstractions;
using TestAssignment.Constants;
using TestAssignment.Services;
using TestAssignment.ViewModels;

namespace TestAssignment.Controllers
{
    [Route("/api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IHubContext<ChatHub> _chatHub;
        private readonly UserManager<UserEntity> _userManager;
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly IJwtService _jwtService;
        private readonly IChatService _queueService;

        public AuthenticationController(
            UserManager<UserEntity> userManager,
            SignInManager<UserEntity> signInManager,
            IJwtService jwtService,

            IHubContext<ChatHub> chatHub,
            IChatService queueService)
        {
            _chatHub = chatHub;
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtService = jwtService;


            _queueService = queueService;
        }


        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Test(CancellationToken cancellationToken)
        {

            try
            {
                var securityIdentifierClaim = User.FindFirstValue(ClaimTypes.Sid);
                var tokenId = Guid.Parse(securityIdentifierClaim);
                await _queueService.SendMessageAsync(tokenId, tokenId, "message", cancellationToken);
            }
            catch (Exception ex)
            {

            }
            //await _chatHub.Clients.All.SendAsync("Send", new object[] { "Test Message" }, cancellationToken);
            return Ok("Hello World!");
        }

        [HttpPost("sign-up")]
        public async Task<IActionResult> SignUp([FromBody] SignUpRequest signUpRequest, CancellationToken cancellationToken)
        {
            var user = new UserEntity
            {

                UserName = signUpRequest.Email,
                Email = signUpRequest.Email,
                NickName = signUpRequest.NickName
            };

            var creatingResult = await _userManager.CreateAsync(user, signUpRequest.Password);

            if (!creatingResult.Succeeded)
            {
                return BadRequest();
            }


            var signInResult = await _signInManager.PasswordSignInAsync(signUpRequest.Email, signUpRequest.Password, false, true);

            if (!signInResult.Succeeded)
            {
                return BadRequest();
            }

            user = await _userManager.FindByEmailAsync(signUpRequest.Email);

            var (accesToken, refreshToken) = await _jwtService.GenerateTokensAsync(user, cancellationToken);


            return Ok(new SignInResponse(accesToken, refreshToken));
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignIn([FromBody] SignInRequest signUpRequest, CancellationToken cancellationToken)
        {
            var signInResult = await _signInManager.PasswordSignInAsync(signUpRequest.Email, signUpRequest.Password, false, true);

            if (!signInResult.Succeeded)
            {
                return BadRequest();
            }

            var user = await _userManager.FindByEmailAsync(signUpRequest.Email);
            var (accesToken, refreshToken) = await _jwtService.GenerateTokensAsync(user, cancellationToken);


            return Ok(new SignInResponse(accesToken, refreshToken));
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh(CancellationToken cancellationToken)
        {
            var securityIdentifierClaim = User.FindFirstValue(ClaimNames.RefreshTokenSid);

            if (securityIdentifierClaim is null)
            {
                return Unauthorized();
            }

            var validTokenId = Guid.TryParse(securityIdentifierClaim, out var tokenId);

            if (!validTokenId)
            {
                return Unauthorized();
            }

            var (accesToken, refreshToken) = await _jwtService.RefreshTokensAsync(tokenId, cancellationToken);

            return Ok(new SignInResponse(accesToken, refreshToken));
        }

        [Authorize]
        [HttpDelete("logout")]
        public async Task<IActionResult> Logout(CancellationToken cancellationToken)
        {
            var securityIdentifierClaim = User.FindFirstValue(ClaimNames.RefreshTokenSid);
            var tokenId = Guid.Parse(securityIdentifierClaim);

            await _jwtService.DeactivateRefreshTokenAsync(tokenId, cancellationToken);

            return Ok();
        }
    }
}
