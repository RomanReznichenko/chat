﻿using DataLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using TestAssignment.Abstractions;
using TestAssignment.ViewModels;

namespace TestAssignment.Controllers
{
    [Authorize]
    [Route("/api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<UserEntity> _userManager;
        private readonly IActiveUsersService _activeUsersService;

        public UsersController(
            UserManager<UserEntity> userManager,
            IActiveUsersService activeUsersService)
        {
            _userManager = userManager;
            _activeUsersService = activeUsersService;
        }

        [HttpGet("me")]
        public async Task<IActionResult> GetMe(CancellationToken cancellationToken)
        {
            var userId = GetCurrentUserId();
            var user = await _userManager.FindByIdAsync(userId.ToString());

            return Ok(new UserResponse(user));
        }

        [HttpGet("active")]
        public async Task<IActionResult> GetActiveUsers(CancellationToken cancellationToken)
        {
            var userId = GetCurrentUserId();
            var users = await _activeUsersService.GetActiveUsersWithoutUser(userId, cancellationToken);
            var response = users.Select(u => new UserResponse(u.User));

            return Ok(response);
        }

        [HttpGet("{id:Guid}/active")]
        public async Task<IActionResult> GetActiveUser([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());

            return Ok(new UserResponse(user));
        }

        private Guid GetCurrentUserId()
        {
            var securityIdentifierClaim = User.FindFirstValue(ClaimTypes.Sid);
            
            return Guid.Parse(securityIdentifierClaim);
        }
    }
}
